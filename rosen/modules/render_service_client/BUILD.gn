# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")
import("render_service_client.gni")

config("render_service_client_config") {
  include_dirs = [
    "$graphic_2d_root/rosen/modules",
    "$graphic_2d_root/rosen/modules/render_service_client/core",
    "$graphic_2d_root/rosen/modules/render_service_base/include",
    "$graphic_2d_root/interfaces/inner_api/common",
    "$graphic_2d_root/interfaces/inner_api/surface",
    "$graphic_2d_root/interfaces/inner_api/composer",
    "//drivers/peripheral/display/interfaces/include",
    "//drivers/peripheral/base",
  ]
  defines = rs_common_define
}

template("render_service_client_source_set") {
  ohos_source_set(target_name) {
    defines = []
    defines += gpu_defines
    defines += [ "MODULE_RSC" ]
    is_static_lib = invoker.is_static_lib
    include_dirs = [
      "$graphic_2d_root/rosen/modules/2d_graphics/src",
      "$graphic_2d_root/rosen/modules/render_frame_trace/include",
      "$graphic_2d_root/rosen/modules/render_service_base/src",
      "$graphic_2d_root/utils/log",
      "$graphic_2d_root/utils/sandbox",
    ]

    sources = [
      #animation
      "core/animation/rs_animation.cpp",
      "core/animation/rs_animation_callback.cpp",
      "core/animation/rs_animation_group.cpp",
      "core/animation/rs_animation_timing_curve.cpp",
      "core/animation/rs_curve_animation.cpp",
      "core/animation/rs_implicit_animation_param.cpp",
      "core/animation/rs_implicit_animator.cpp",
      "core/animation/rs_implicit_animator_map.cpp",
      "core/animation/rs_interpolating_spring_animation.cpp",
      "core/animation/rs_keyframe_animation.cpp",
      "core/animation/rs_path_animation.cpp",
      "core/animation/rs_property_animation.cpp",
      "core/animation/rs_spring_animation.cpp",
      "core/animation/rs_transition.cpp",
      "core/animation/rs_transition_effect.cpp",

      #modifier
      "core/modifier/rs_extended_modifier.cpp",
      "core/modifier/rs_modifier.cpp",
      "core/modifier/rs_modifier_extractor.cpp",
      "core/modifier/rs_modifier_manager.cpp",
      "core/modifier/rs_modifier_manager_map.cpp",
      "core/modifier/rs_property.cpp",
      "core/modifier/rs_property_modifier.cpp",

      #jank_detector
      "core/jank_detector/rs_jank_detector.cpp",

      #pipeline
      "core/pipeline/rs_divided_ui_capture.cpp",
      "core/pipeline/rs_node_map.cpp",
      "core/pipeline/rs_render_thread.cpp",
      "core/pipeline/rs_render_thread_visitor.cpp",

      #transaction
      "core/transaction/rs_application_agent_impl.cpp",
      "core/transaction/rs_interfaces.cpp",
      "core/transaction/rs_process_transaction_controller.cpp",
      "core/transaction/rs_render_thread_client.cpp",
      "core/transaction/rs_sync_transaction_controller.cpp",
      "core/transaction/rs_transaction.cpp",

      #ui
      "core/ui/rs_base_node.cpp",
      "core/ui/rs_canvas_node.cpp",
      "core/ui/rs_display_node.cpp",
      "core/ui/rs_node.cpp",
      "core/ui/rs_proxy_node.cpp",
      "core/ui/rs_root_node.cpp",
      "core/ui/rs_surface_extractor.cpp",
      "core/ui/rs_surface_node.cpp",
      "core/ui/rs_ui_director.cpp",
      "core/ui/rs_ui_share_context.cpp",
    ]

    if (defined(graphic_2d_ext_configs.vendor_root)) {
      sources += graphic_2d_ext_configs.librender_service_client_ext_sources
      sources -= graphic_2d_ext_configs.librender_service_client_ext_sources_del
    }

    cflags = [
      "-Wall",
      "-Wno-pointer-arith",
      "-Wno-non-virtual-dtor",
      "-Wno-missing-field-initializers",
      "-Wno-c++11-narrowing",
      "-fvisibility=hidden",
    ]

    cflags_cc = [
      "-std=c++17",
      "-fvisibility-inlines-hidden",
    ]

    deps = [
      "$graphic_2d_root/rosen/modules/platform:ipc_core",
      "$graphic_2d_root/rosen/modules/render_frame_trace:render_frame_trace",
    ]

    if (enable_export_macro) {
      defines += [ "ENABLE_EXPORT_MACRO" ]
    }

    if (rosen_is_ohos) {
      deps +=
          [ "$graphic_2d_root/rosen/modules/frame_analyzer:libframe_analyzer" ]
      external_deps = [
        "c_utils:utils",
        "hisysevent_native:libhisysevent",
        "hitrace_native:hitrace_meter",
      ]

      if (accessibility_enable) {
        external_deps += [ "accessibility:accessibilityconfig" ]
        defines += accessibility_defines
      }
    } else if (rosen_preview) {
      deps += [
        "$graphic_2d_root:libgl",
        "$graphic_2d_root/rosen/modules/glfw_render_context:libglfw_render_context",
      ]

      if (rss_enabled) {
        external_deps = []
      }
    }

    if (rss_enabled) {
      defines += [ "OHOS_RSS_CLIENT" ]
      external_deps += [ "resource_schedule_service:ressched_client" ]
    }

    if (is_static_lib) {
      public_deps = [ "$graphic_2d_root/rosen/modules/render_service_base:librender_service_base_static" ]
    } else {
      public_deps = [ "$graphic_2d_root/rosen/modules/render_service_base:librender_service_base" ]
      deps += [ "$rosen_root/modules/platform:eventhandler" ]
    }

    public_configs = [ ":render_service_client_config" ]
  }
}

render_service_client_source_set("render_service_client_src") {
  is_static_lib = false
}

render_service_client_source_set("render_service_client_src_static") {
  is_static_lib = true
}

ohos_shared_library("librender_service_client") {
  public_deps = [ ":render_service_client_src" ]
  part_name = "graphic_standard"
  subsystem_name = "graphic"
}

ohos_source_set("librender_service_client_static") {
  public_deps = [ ":render_service_client_src_static" ]
  deps = [ "$rosen_root/modules/platform:eventhandler" ]
  part_name = "graphic_standard"
  subsystem_name = "graphic"
}

group("test") {
  if (rosen_is_ohos) {
    testonly = true

    deps = [ "test:test" ]
  }
}
