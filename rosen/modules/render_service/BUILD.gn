# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")

## Build librender_service.so
ohos_shared_library("librender_service") {
  defines = []
  if (graphic_standard_feature_color_gamut_enable) {
    defines += [ "PADDING_HEIGHT_32" ]
  } else {
    defines += []
  }
  if (use_musl) {
    defines += [ "FRAME_AWARE_TRACE" ]
  }
  defines += gpu_defines

  sources = [
    "core/memory/MemoryManager.cpp",
    "core/memory/SkiaMemoryTracer.cpp",
    "core/pipeline/rs_base_render_engine.cpp",
    "core/pipeline/rs_base_render_util.cpp",
    "core/pipeline/rs_cold_start_thread.cpp",
    "core/pipeline/rs_composer_adapter.cpp",
    "core/pipeline/rs_divided_render_util.cpp",
    "core/pipeline/rs_hardware_thread.cpp",
    "core/pipeline/rs_main_thread.cpp",
    "core/pipeline/rs_physical_screen_processor.cpp",
    "core/pipeline/rs_processor.cpp",
    "core/pipeline/rs_processor_factory.cpp",
    "core/pipeline/rs_qos_thread.cpp",
    "core/pipeline/rs_render_engine.cpp",
    "core/pipeline/rs_render_service.cpp",
    "core/pipeline/rs_render_service_connection.cpp",
    "core/pipeline/rs_render_service_listener.cpp",
    "core/pipeline/rs_render_service_visitor.cpp",
    "core/pipeline/rs_surface_capture_task.cpp",
    "core/pipeline/rs_uni_render_composer_adapter.cpp",
    "core/pipeline/rs_uni_render_engine.cpp",
    "core/pipeline/rs_uni_render_judgement.cpp",
    "core/pipeline/rs_uni_render_listener.cpp",
    "core/pipeline/rs_uni_render_processor.cpp",
    "core/pipeline/rs_uni_render_util.cpp",
    "core/pipeline/rs_uni_render_virtual_processor.cpp",
    "core/pipeline/rs_uni_render_visitor.cpp",
    "core/pipeline/rs_uni_ui_capture.cpp",
    "core/pipeline/rs_unmarshal_thread.cpp",
    "core/pipeline/rs_virtual_screen_processor.cpp",
    "core/screen_manager/rs_screen.cpp",
    "core/screen_manager/rs_screen_manager.cpp",
    "core/system/rs_system_parameters.cpp",
    "core/transaction/rs_render_service_connection_stub.cpp",
    "core/transaction/rs_render_service_stub.cpp",
  ]

  if (rs_enable_driven_render && rs_enable_gpu) {
    sources += [
      "core/pipeline/driven_render/rs_driven_render_ext.cpp",
      "core/pipeline/driven_render/rs_driven_render_listener.cpp",
      "core/pipeline/driven_render/rs_driven_render_manager.cpp",
      "core/pipeline/driven_render/rs_driven_render_visitor.cpp",
      "core/pipeline/driven_render/rs_driven_surface_render_node.cpp",
    ]
  }

  if (rs_enable_eglimage) {
    sources += [ "core/pipeline/rs_egl_image_manager.cpp" ]
  }

  if (rs_enable_parallel_render && rs_enable_gpu) {
    sources += [
      "core/pipeline/parallel_render/rs_node_cost_manager.cpp",
      "core/pipeline/parallel_render/rs_parallel_hardware_composer.cpp",
      "core/pipeline/parallel_render/rs_parallel_pack_visitor.cpp",
      "core/pipeline/parallel_render/rs_parallel_render_manager.cpp",
      "core/pipeline/parallel_render/rs_parallel_sub_thread.cpp",
      "core/pipeline/parallel_render/rs_parallel_task_manager.cpp",
      "core/pipeline/parallel_render/rs_render_task.cpp",
    ]
  }

  include_dirs = [
    "core",
    "//foundation/graphic/graphic_2d/frameworks/surface/include",
    "//foundation/graphic/graphic_2d/rosen/include",
    "//foundation/graphic/graphic_2d/rosen/modules/composer/vsync/include",
    "$graphic_2d_root/rosen/modules/render_frame_trace/include",
    "//foundation/graphic/graphic_2d/rosen/modules/render_service_base/src",
    "//foundation/graphic/graphic_2d/rosen/modules/utils",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "//foundation/graphic/graphic_2d/utils/log",
    "//foundation/barrierfree/accessibility/interfaces/innerkits/acfwk/include",
  ]

  deps = [
    "$graphic_2d_root/rosen/modules/render_frame_trace:render_frame_trace",
    "//foundation/graphic/graphic_2d:libsurface",
    "//foundation/graphic/graphic_2d/rosen/modules/composer:libcomposer",
    "//foundation/graphic/graphic_2d/rosen/modules/render_service_base:librender_service_base",
    "//foundation/systemabilitymgr/safwk/interfaces/innerkits/safwk:system_ability_fwk",
  ]

  public_deps = [
    "//third_party/flutter/build/skia:ace_skia_ohos",
    "//third_party/libpng:libpng",
  ]

  if (rs_enable_gpu) {
    include_dirs += [ "$graphic_2d_root/frameworks/vulkan_wrapper/include" ]

    if (graphic_standard_feature_enable_vulkan) {
      include_dirs += [ "//third_party/flutter/engine/flutter/vulkan" ]
    }

    public_deps += [ "$graphic_2d_root:libvulkan" ]
  }

  external_deps = [
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hicollie_native:libhicollie",
    "hisysevent_native:libhisysevent",
    "hitrace_native:hitrace_meter",
    "init:libbegetutil",
    "ipc:ipc_core",
    "samgr:samgr_proxy",
  ]

  if (defined(global_parts_info) &&
      defined(global_parts_info.resourceschedule_soc_perf)) {
    external_deps += [
      "resource_schedule_service:ressched_client",
      "soc_perf:socperf_client",
    ]
    defines += [
      "SOC_PERF_ENABLE",
      "RES_SCHED_ENABLE",
    ]
  }

  if (accessibility_enable) {
    external_deps += [ "accessibility:accessibilityconfig" ]
    defines += accessibility_defines
  }

  part_name = "graphic_standard"
  subsystem_name = "graphic"
}

## Build render_service.bin
ohos_executable("render_service") {
  sources = [ "core/main.cpp" ]

  include_dirs = [
    "core",
    "//commonlibrary/c_utils/base/include",
  ]

  deps = [
    ":librender_service",
    "//foundation/graphic/graphic_2d/rosen/modules/composer:libcomposer",
    "//foundation/graphic/graphic_2d/rosen/modules/render_service_base:librender_service_base",
  ]

  part_name = "graphic_standard"
  subsystem_name = "graphic"
}
